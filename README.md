# LoRa2MQTT

> Converts CBOR LoRa messages to MQTT.

## Expected input

Any valid CBOR message.

## Expected output

```json
{
    "size": 42, // size of the CBOR message
    "msg": {
        // content of the CBOR message in jSON
        // ...
    },
    "rssi": -42
}
```

## Usage

Add a `secrets.h` file in [`include/`](include/) e.g.

```h
w#define BROKER_PORT 1883

#ifdef ESP32
#ifdef INRAE
#define TTLS_IDENTITY "user"
#define TTLS_PASSWORD "P4ssw0rd!"
#define TTLS_USERNAME "user"
#else
#define SSID "wifi_name"
#define PASSWORD "wifi_password"
#endif
#elif defined(MKR)
// change to match your network
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
byte ip[] = {192, 168, 1, 177};  
#endif
```

Then upload the sketch: 

```
pio run -t upload
```

## MQTT API

LoRa2MQTT operates on the topic `lora2mqtt`. 

- node or messages are distributed on `lora2mqtt/{node_name,anonymous}` depending on the presence of `nid` key in the CBOR message.
- Config is handled on `lora2mqtt/config`
    - `lora2mqtt/config/state` will output the current config, e.g.
        ```json
        {
            "frequency": 868000000,
            "lna_gain": 6,
            "device": "Firebeetle_ESP32"
        }
        ```
    - publishing on `lora2mqtt/config/set` allows to change the current config, e.g.
        ```json
        {
            "lna_gain": 5,
            "frequency": 868100000
        }
        ```
    - publishing on `lora2mqtt/config/reset` allows to 

## Configuration and dependencies

See [`config.h`](include/config.h) and [`platformio.ini`](include/platformio.ini).

## Improvements

- support MQTT authentication
- improve stability
- CI/CD
- send LoRa CBOR messages to nodes, e.g. acknoledgements and update sender configs that doesn't require to change the receiver settings, e.g.
    ```json
    { 
        "spreading_factor": 12,
        "bandwidth": 125000,
        "coding_rate_denominator": 8,
        "crc": true
    }
    ```
- urdflib support?
- ...