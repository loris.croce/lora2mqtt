#include "CStringPrint.h"

// Constructor
CStringPrint::CStringPrint(size_t size)
{
    bufferSize = size;
    buffer = new char[bufferSize];
    clear();
}

// Destructor
CStringPrint::~CStringPrint()
{
    delete[] buffer;
}

// Overriding the write method
size_t CStringPrint::write(uint8_t character)
{
    if (length < bufferSize - 1)
    { // Ensure there's space for the new character and null terminator
        buffer[length++] = (char)character;
        buffer[length] = '\0'; // Null-terminate the string
        return 1;
    }
    else
    {
        return 0; // No space left in the buffer
    }
}

// Overriding the write method for strings
size_t CStringPrint::write(const uint8_t *buf, size_t size)
{
    size_t written = 0;
    for (size_t i = 0; i < size; i++)
    {
        written += write(buf[i]);
    }
    return written;
}

// Method to get the stored cstring
const char *CStringPrint::getCString()
{
    return buffer;
}

// Method to clear the stored string
void CStringPrint::clear()
{
    length = 0;
    buffer[0] = '\0'; // Null-terminate the string
}