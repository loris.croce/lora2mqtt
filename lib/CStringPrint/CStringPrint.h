#ifndef CSTRINGPRINT_H
#define CSTRINGPRINT_H

#include <Arduino.h>

class CStringPrint : public Print
{
private:
    char *buffer;
    size_t bufferSize;
    size_t length;

public:
    // Constructor
    CStringPrint(size_t size);

    // Destructor
    ~CStringPrint();

    // Overriding the write method
    virtual size_t write(uint8_t character) override;

    // Overriding the write method for strings
    virtual size_t write(const uint8_t *buf, size_t size) override;

    // Method to get the stored cstring
    const char *getCString();

    // Method to clear the stored string
    void clear();
};

#endif // CSTRINGPRINT_H
