#include <SPI.h>
#include <LoRa.h>
#include <tinycbor.h>
#include <ArduinoJson.h>
#ifdef ESP32
#include <WiFi.h>
#elif defined(MKR)
#include <Ethernet.h>
#endif
#include <ArduinoMqttClient.h>
#include <CStringPrint.h>

#include "secrets.h"
#include "config.h"

#ifdef ESP32
WiFiClient net;
#elif defined(MKR)
EthernetClient net;
#endif
MqttClient client(net);

char topic[TOPIC_SIZE];
char mqttPayload[PAYLOAD_SIZE];

bool configSend = true;

long frequency;
int lna_gain;

#ifdef ESP32
const char *device = "Firebeetle_ESP32";
#elif defined(MKR)
const char *device = "Arduino_MKR1310";
#endif

void initLoRa()
{
#ifdef ESP32
  LoRa.setPins(NSS, NRESET);
#endif
  LoRa.setGain(lna_gain);

  if (!LoRa.begin(frequency))
  {
    while (1)
    {
      Serial.println("Starting LoRa failed!");
      delay(2000);
    }
  }

  LoRa.receive();
}

void connectMQTT()
{
#ifdef ESP32
#ifdef INRAE
  WiFi.begin(SSID, WPA2_AUTH_TTLS, TTLS_IDENTITY, TTLS_USERNAME, TTLS_PASSWORD);
#else
  WiFi.begin(SSID, PASSWORD);
#endif
  Serial.print("checking wifi...");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(1000);
  }
#elif defined(MKR)
  Ethernet.init(5);
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin((uint8_t *)mac) == 0)
  {
    Serial.println("Failed to configure Ethernet using DHCP");
    if (Ethernet.hardwareStatus() == EthernetNoHardware)
    {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
      while (true)
      {
        delay(1);
      }
    }
    if (Ethernet.linkStatus() == LinkOFF)
    {
      Serial.println("Ethernet cable is not connected.");
    }
    Ethernet.begin((uint8_t *)mac, ip);
  }
  else
  {
    Serial.print("  DHCP assigned IP ");
    Serial.println(Ethernet.localIP());
  }

#endif

  Serial.print("\nconnecting...");
  client.setId(device);
  while (!client.connect(BROKER_ADDRESS, BROKER_PORT))
  {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");
  client.subscribe("lora2mqtt/config/#");
}

void messageReceived(int messageSize)
{
  if (client.messageTopic().endsWith("/set"))
  {
    JsonDocument config;
    String payload;
    while (client.available())
    {
      payload += (char)client.read();
    }
    deserializeJson(config, payload);

    if (config.containsKey("frequency"))
      frequency = config["frequency"];
    if (config.containsKey("lna_gain"))
      lna_gain = config["lna_gain"];

    LoRa.end();
    initLoRa();
    configSend = true;
  }
  else if (client.messageTopic().endsWith("/reset"))
  {
#ifdef ESP32
    ESP.restart();
#elif defined(MKR)
    NVIC_SystemReset();
#endif
  }
  else if (client.messageTopic().endsWith("/get"))
  {
    configSend = true;
  }
}

void sendConfig()
{
  JsonDocument config;
  config["frequency"] = frequency;
  config["lna_gain"] = lna_gain;
  config["device"] = device;

  strcpy(topic, BASE_TOPIC);
  strcat(topic, "config/state");
  serializeJson(config, mqttPayload);
  client.beginMessage(topic);
  client.print(mqttPayload);
  client.endMessage();
  strcpy(topic, "");
  strcpy(mqttPayload, "");
  configSend = false;
}

void sendMessage(int packetSize)
{
  uint8_t msg_buf[packetSize] = {0};
  int i = 0;

  while (LoRa.available())
  {
    msg_buf[i] = (uint8_t)LoRa.read();
    i++;
  }

  TinyCBOR.init();
  TinyCBOR.Parser.init(msg_buf, packetSize, 0);
  CStringPrint cstringPrint(MSG_SIZE);
  TinyCBOR.Parser.to_json(cstringPrint);

  JsonDocument json_message;
  json_message["size"] = packetSize;
  JsonObject msg = json_message["msg"].to<JsonObject>();
  deserializeJson(json_message["msg"], cstringPrint.getCString());
  cstringPrint.clear();

  json_message["rssi"] = LoRa.packetRssi();
  strcpy(topic, BASE_TOPIC);

  if (msg["nid"] && (msg["nid"] != "config"))
  {
    strcat(topic, msg["nid"]);
    msg.remove("nid");
  }
  else
  {
    strcat(topic, "anonymous");
  }

  serializeJson(json_message, mqttPayload);
  client.beginMessage(topic);
  client.print(mqttPayload);
  client.endMessage();
  strcpy(topic, "");
  strcpy(mqttPayload, "");
}

void setup()
{
  Serial.begin(115200);

  frequency = FREQUENCY;
  lna_gain = LNA_GAIN;

  initLoRa();

  client.onMessage(messageReceived);
  connectMQTT();
}

void loop()
{
  client.poll();
  delay(10); // <- fixes some issues with WiFi stability (and LoRa!)

#ifdef ESP32
  if (!client.connected() && WiFi.status() != WL_CONNECTED)
#elif defined(MKR)
  if (!client.connected())
#endif
    connectMQTT();

  if (configSend)
    sendConfig();

  int packetSize = LoRa.parsePacket();

  if (packetSize)
    sendMessage(packetSize);
}